#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>


#ifndef FLOYD_GRAPH
#define FLOYD_GRAPH
class Helper {
public:
	std::vector<int> prefixFunctionFinding(const std::string& str);

	std::string getAnswer(std::vector<int>& prefexFunction, int templLenght);
};
#endif

