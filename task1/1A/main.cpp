#include <gtest\gtest.h>
#include <gmock\gmock.h>
#include "Helper.h"
using namespace std;

TEST(AlgoTest, 1test) {
	std::string templ = "a";
	std::string str = "aaa";
	Helper hl;
	std::vector<int> prefexFunction = hl.prefixFunctionFinding(templ + '#' + str);
	ASSERT_EQ(hl.getAnswer(prefexFunction, templ.length()), "0 1 2 ");

}

TEST(AlgoTest, 2test) {
	std::string templ = "ab";
	std::string str = "aaa";
	Helper hl;
	std::vector<int> prefexFunction = hl.prefixFunctionFinding(templ + '#' + str);
	ASSERT_EQ(hl.getAnswer(prefexFunction, templ.length()), "");

}

TEST(AlgoTest, 3test) {
	std::string templ = "aba";
	std::string str = "abababa";
	Helper hl;
	std::vector<int> prefexFunction = hl.prefixFunctionFinding(templ + '#' + str);
	ASSERT_EQ(hl.getAnswer(prefexFunction, templ.length()), "0 2 4 ");

}
/*
int main(int argc, char const *argv[]) {
	std::string templ;
	std::string str;
	std::cin >> templ >> str;
	Helper hl;

	std::vector<int> prefexFunction = hl.prefixFunctionFinding(templ + '#' + str);
	hl.getAnswer(prefexFunction, templ.length());
	//system("pause");
	return 0;
}*/

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
