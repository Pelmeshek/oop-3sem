#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include "Helper.h"

std::vector<int> Helper::prefixFunctionFinding(const std::string& str) {
	std::vector<int> prefexFunction(str.length(), 0);
	for (size_t i = 1; i < str.length(); i++) {
		int curr = prefexFunction[i - 1];
		while (curr > 0 && str[i] != str[curr]) {
			curr = prefexFunction[curr - 1];
		}
		if (str[i] == str[curr])  ++curr;
		prefexFunction[i] = curr;
	}
	return prefexFunction;
}

std::string Helper::getAnswer(std::vector<int>& prefexFunction, int templLenght) {
	std::stringstream ss;
	for (size_t i = templLenght + 1; i < prefexFunction.size(); i++) {
		if (prefexFunction[i] == templLenght) {
			ss <<  i - templLenght * 2 << ' ';// << templLenght << ' ' << i << std::endl;
		}
	}
	std::string answ = ss.str();
	return answ;
}