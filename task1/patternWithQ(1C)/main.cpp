#include "Header.h"

int main() {
	std::string pattern;
	std::string text;
	std::cin >> pattern >> text;
	if (pattern.size() > text.size()) {
		return 0;
	}
	std::vector<std::string> splitPattern(0);
	std::vector<int> startPositions(0);
	std::string s;
	int i = 0;
	for (; i < pattern.length(); ++i) {
		if (pattern[i] != '?') {
			s += pattern[i];
		}
		else {
			if (s.length() > 0) {
				splitPattern.push_back(s);
				startPositions.push_back(i - s.length());
				s.clear();
			}
		}
	}
	if (s.length() > 0) {
		splitPattern.push_back(s);
		startPositions.push_back(i - s.length());
	}
	std::vector<chain> trie = makeTrie(splitPattern, startPositions);

	std::vector<int> positions = findPositions(trie, text, pattern, splitPattern);
	for (int x : positions) {
		std::cout << x << ' ';
	}
	return 0;
}
