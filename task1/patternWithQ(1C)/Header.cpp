#include "Header.h"

std::vector<chain> makeTrie(const std::vector<std::string>& splitPattern, const std::vector<int>& startPositions) {
	std::vector<chain> trie;
	chain root(0, -1);
	trie.push_back(root);
	for (int i = 0; i < splitPattern.size(); ++i) {
		int current = 0;
		for (char c : splitPattern[i]) {
			if (trie[current].children[c - 97] == -1) {
				chain temp(current, c - 97);
				trie.push_back(temp);
				int number = trie.size() - 1;
				trie[current].children[c - 97] = number;
			}
			current = trie[current].children[c - 97];
		}
		trie[current].leaf = true;
		trie[current].pos.push_back(startPositions[i] + splitPattern[i].length() - 1);
	}
	return trie;
}

int getLink(std::vector<chain>& trie, int node, int _char) {
	if (trie[node].bridge[_char] == -1) {
		if (trie[node].children[_char] != -1) {
			trie[node].bridge[_char] = trie[node].children[_char];
		}
		else {
			if (node == 0) {
				trie[node].bridge[_char] = 0;
			}
			else {
				trie[node].bridge[_char] = getLink(trie, getSuffixLink(trie, node), _char);
			}
		}
	}
	return trie[node].bridge[_char];
}


int getSuffixLink(std::vector<chain>& trie, int node) {
	if (trie[node].suffixLink == -1) {
		if (node == 0 || trie[node].preceq == 0) {
			trie[node].suffixLink = 0;
		}
		else {
			trie[node].suffixLink = getLink(trie, getSuffixLink(trie, trie[node].preceq), trie[node].charToParent);
		}
	}
	return trie[node].suffixLink;
}

int getTerminateLink(std::vector<chain>& trie, int node) {
	if (trie[node].terminateLink == -1) {
		int v = getSuffixLink(trie, node);
		if (trie[v].leaf == true) {
			trie[node].terminateLink = v;
		}
		else {
			if (v == 0) {
				trie[node].terminateLink = 0;
			}
			else {
				trie[node].terminateLink = getTerminateLink(trie, v);
			}
		}
	}
	return trie[node].terminateLink;
}

std::vector<int> findPositions(std::vector<chain>& trie, const std::string& text, const std::string& pattern, const std::vector<std::string>& splitPattern) {
	std::vector<int> result(0);
	std::vector<int> C(text.length() - pattern.length() + 1, 0);
	int current = 0;
	for (int i = 0; i < text.size(); ++i) {
		int c = text[i] - 97;
		current = getLink(trie, current, c);
		int tmp = current;
		while (tmp > 0) {
			if (trie[tmp].leaf == true) {
				for (int p : trie[tmp].pos) {
					if (i - p >= 0 && i - p < C.size()) {
						++C[i - p];
						if (C[i - p] == splitPattern.size()) {
							result.push_back(i - p);
							break;
						}
					}
				}

			}
			tmp = getTerminateLink(trie, tmp);
		}
	}
	return result;
}