#pragma once

#include <iostream>
#include <string>
#include <vector>
#ifndef Header
#define Header

struct chain {
	std::vector<int> children;
	std::vector<int> bridge;
	int preceq;
	int suffixLink;
	int terminateLink;
	int charToParent;
	bool leaf;
	std::vector<int> pos;
	chain(int par = -1, int chToPar = -1) : preceq(par), suffixLink(-1), terminateLink(-1), charToParent(chToPar), leaf(false) {
		children.resize(26, -1);
		bridge.resize(26, -1);
		pos.resize(0);
	};
};

std::vector<chain> makeTrie(const std::vector<std::string>& splitPattern, const std::vector<int>& startPositions);
int getLink(std::vector<chain>& trie, int node, int _char);
int getSuffixLink(std::vector<chain>& trie, int node);
int getTerminateLink(std::vector<chain>& trie, int node);
std::vector<int> findPositions(std::vector<chain>& trie, const std::string& text, const std::string& pattern, const std::vector<std::string>& splitPattern);
#endif 