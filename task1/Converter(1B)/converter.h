#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>


#ifndef FLOYD_GRAPH
#define FLOYD_GRAPH
struct converter {
	std::vector<int> strToPrefix(const std::string& str);
	std::vector<int> strToZ(const std::string& str);
	std::string prefixToStr(std::vector<int>& prefixFunc);
	std::vector<int> prefixToZ(std::vector<int>& prefixFunc);
	std::vector<int> zToPrefix(std::vector<int>& zFunc);
	std::string zToStr(std::vector<int>& zFunc);
};
#endif

