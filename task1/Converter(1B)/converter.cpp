#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include "converter.h"

std::vector<int> converter::strToPrefix(const std::string& str) {
	std::vector<int> prefixFunction(str.length(), 0);
	for (size_t i = 1; i < str.length(); i++) {
		int curr = prefixFunction[i - 1];
		while (curr > 0 && str[i] != str[curr]) {
			curr = prefixFunction[curr - 1];
		}
		if (str[i] == str[curr])  ++curr;
		prefixFunction[i] = curr;
	}
	return prefixFunction;
}

std::vector<int> converter::strToZ(const std::string& str) {
	std::vector<int> zFunc(str.length(), 0);
	int left = 0;
	int right = 0;
	for (int i = 1; i < str.length(); i++) {
		zFunc[i] = std::max(0, std::min(right - i, zFunc[i - left]));
		while (i + zFunc[i] < str.length() && str[zFunc[i]] == str[i + zFunc[i]]) {
			zFunc[i]++;
		}
		if (i + zFunc[i] >= right) {
			left = i;
			right = i + zFunc[i];
		}
	}
	return zFunc;
}

std::string converter::prefixToStr(std::vector<int>& prefixFunction) {
	std::string str = "";
	char c = 'a';
	if (prefixFunction.size()) {
		str += 'a';
	}
	for (size_t i = 1; i < prefixFunction.size(); i++) {
		if (prefixFunction[i] == 0) {
			for (char ch = 'b'; ch != c + 1; ch++) {
				int curr = prefixFunction[i - 1];
				while (curr > 0 && ch != str[curr]) {
					curr = prefixFunction[curr - 1];
				}
				if (ch != str[curr]) {
					str += ch;
					break;
				}
			}
			if (str.length() != i + 1)
				str += ++c;
		}
		else {
			char ch = str[prefixFunction[i] - 1];
			str += ch;
		}
	}
	return str;
}

std::vector<int> converter::prefixToZ(std::vector<int>& prefixFunc) {
	std::string str = prefixToStr(prefixFunc);
	std::vector<int> zFunc = strToZ(str);
	return zFunc;
}

std::vector<int> converter::zToPrefix(std::vector<int>& zFunc) {
	std::vector<int> prefixFunc(zFunc.size(), 0);
	for (size_t i = 1; i < zFunc.size(); i++) {
		for (int j = zFunc[i] - 1; j >= 0; j--) {
			if (prefixFunc[i + j] > 0) {
				break;
			}
			else {
				prefixFunc[i + j] = j + 1;
			}
		}
	}
	return prefixFunc;
}

std::string converter::zToStr(std::vector<int>& zFunc) {
	std::vector<int> prefixFunc = zToPrefix(zFunc);
	std::string str = prefixToStr(prefixFunc);
	return str;
}