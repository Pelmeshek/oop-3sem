#include <gtest\gtest.h>
#include <gmock\gmock.h>
#include "converter.h"
using namespace std;

TEST(strToZTest, 1test) {
	std::string str = "ababbabac";
	converter conv;
	std::vector<int> zFunc = conv.strToZ(str);
	std::vector<int> answ = {0,0,2,0,0,3,0,1,0};
	EXPECT_EQ(zFunc.size(), answ.size());
	for (size_t i = 0; i < zFunc.size(); i++){
		EXPECT_EQ(zFunc[i], answ[i]);
	}
	
}

TEST(strToPrefix, 1test) {
	std::string str = "ababbabac";
	converter conv;
	std::vector<int> zFunc = conv.strToPrefix(str);
	std::vector<int> answ = { 0,0,1,2,0,1,2,3,0 };
	EXPECT_EQ(zFunc.size(), answ.size());
	for (size_t i = 0; i < zFunc.size(); i++) {
		EXPECT_EQ(zFunc[i], answ[i]);
	}

}

TEST(prefixToStr, 1test) {
	std::vector<int> prefix = { 0,0,1,2,0,1,2,3,0 };
	std::string answer = "ababbabac";
	converter conv;
	EXPECT_EQ(answer, conv.prefixToStr(prefix));
}

TEST(zToStr, 1test) {
	std::vector<int> zFunc = { 0,0,2,0,0,3,0,1,0 };
	std::string answer = "ababbabac";
	converter conv;
	EXPECT_EQ(answer, conv.zToStr(zFunc));
}


TEST(zToPrefix, 1test) {
	std::vector<int> zFunc = { 0,0,2,0,0,3,0,1,0 };
	std::vector<int> prefix = { 0,0,1,2,0,1,2,3,0 };
	converter conv;
	std::vector<int> answer = conv.prefixToZ(prefix);
	EXPECT_EQ(zFunc.size(), answer.size());
	for (size_t i = 0; i < zFunc.size(); i++) {
		EXPECT_EQ(zFunc[i], answer[i]);
	}
}

TEST(prefixToZ, 1test) {
	std::vector<int> zFunc = { 0,0,2,0,0,3,0,1,0 };
	std::vector<int> prefix = { 0,0,1,2,0,1,2,3,0 };
	converter conv;
	std::vector<int> answer = conv.zToPrefix(zFunc);
	EXPECT_EQ(prefix.size(), answer.size());
	for (size_t i = 0; i < prefix.size(); i++) {
		EXPECT_EQ(prefix[i], answer[i]);
	}
}

TEST(strToZTest, 2test) {
	std::string str = "abcabca";
	converter conv;
	std::vector<int> zFunc = conv.strToZ(str);
	std::vector<int> answ = { 0,0,0,4,0,0,1 };
	EXPECT_EQ(zFunc.size(), answ.size());
	for (size_t i = 0; i < zFunc.size(); i++) {
		EXPECT_EQ(zFunc[i], answ[i]);
	}

}

TEST(strToPrefix, 2test) {
	std::string str = "abcabca";
	converter conv;
	std::vector<int> zFunc = conv.strToPrefix(str);
	std::vector<int> answ = { 0,0,0,1,2,3,4};
	EXPECT_EQ(zFunc.size(), answ.size());
	for (size_t i = 0; i < zFunc.size(); i++) {
		EXPECT_EQ(zFunc[i], answ[i]);
	}

}

TEST(prefixToStr, 2test) {
	std::vector<int> prefix = { 0,0,0,1,2,3,4 };
	std::string answer = "abbabba";
	converter conv;
	EXPECT_EQ(answer, conv.prefixToStr(prefix));
}

TEST(zToStr, 2test) {
	std::vector<int> zFunc = { 0,0,0,4,0,0,1 };
	std::string answer = "abbabba";
	converter conv;
	EXPECT_EQ(answer, conv.zToStr(zFunc));
}


TEST(zToPrefix, 2test) {
	std::vector<int> zFunc = { 0,0,0,4,0,0,1 };
	std::vector<int> prefix = { 0,0,0,1,2,3,4 };
	converter conv;
	std::vector<int> answer = conv.prefixToZ(prefix);
	EXPECT_EQ(zFunc.size(), answer.size());
	for (size_t i = 0; i < zFunc.size(); i++) {
		EXPECT_EQ(zFunc[i], answer[i]);
	}
}

TEST(prefixToZ, 2test) {
	std::vector<int> zFunc = { 0,0,0,4,0,0,1 };
	std::vector<int> prefix = { 0,0,0,1,2,3,4 };
	converter conv;
	std::vector<int> answer = conv.zToPrefix(zFunc);
	EXPECT_EQ(prefix.size(), answer.size());
	for (size_t i = 0; i < prefix.size(); i++) {
		EXPECT_EQ(prefix[i], answer[i]);
	}
}



/*
int main(int argc, char const *argv[]) {
	std::string templ;
	std::string str;
	std::cin >> templ >> str;
	Helper hl;

	std::vector<int> prefexFunction = hl.prefixFunctionFinding(templ + '#' + str);
	hl.getAnswer(prefexFunction, templ.length());
	//system("pause");
	return 0;
}*/

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
