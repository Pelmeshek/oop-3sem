#include <gtest\gtest.h>
#include <gmock\gmock.h>
#include "Helper.h"

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <math.h>

TEST(algoTest, test1) {
	std::string str = "abab";
	helper hlp;
	EXPECT_EQ(7, hlp.getAnswer(str));
}

TEST(algoTest, test2) {
	std::string str = "ababacadab";
	helper hlp;
	EXPECT_EQ(45, hlp.getAnswer(str));
}

TEST(algoTest, test3) {
	std::string str = "";
	helper hlp;
	EXPECT_EQ(0, hlp.getAnswer(str));
}

TEST(algoTest, test4) {
	std::string str = "aaaaaaaaa";
	helper hlp;
	EXPECT_EQ(9, hlp.getAnswer(str));
}

TEST(algoTest, test5) {
	std::string str = "aaaaabaaaa";
	helper hlp;
	EXPECT_EQ(35, hlp.getAnswer(str));
}


/*
int main(int argc, char const *argv[]) {
	std::string str;
	std::cin >> str;
	helper hlp;
	
	int answer = hlp.getAnswer(str);
	std::cout << answer << std::endl;
	//system("pause");
	return 0;
}
*/
int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
