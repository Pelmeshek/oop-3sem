#include <iostream>
#include <queue>
#include <sstream>


#ifndef Header
#define Header
const int alfSize = 27;
class helper {
	std::vector<int> strToSuffArr(std::string& str);

	std::vector<int> lcpMassCr(std::string& str, std::vector<int>& suffArr);
public:
	int getAnswer(std::string str);
};

#endif