#include <iostream>
#include <queue>
#include "Helper.h"
#include <sstream>


std::vector<int> helper::strToSuffArr(std::string& str) {

	std::vector<int> suffArr(str.length(), 0);
	std::vector<int> classes(str.length(), 0);
	std::vector<int> letterCounters(alfSize, 0);
	/*-----------------0 ����----------------------*/
	for (size_t i = 0; i < str.length(); i++) {
		letterCounters[str[i] - 'a' + 1]++;
	}
	for (size_t i = 1; i < alfSize; i++) {
		letterCounters[i] += letterCounters[i - 1];
	}
	for (size_t i = 0; i < str.length(); i++) {
		suffArr[--letterCounters[str[i] - 'a' + 1]] = i;
	}
	for (size_t i = 0; i < suffArr.size(); i++)
	{
		//std::cout << suffArr[i] << ' ';
	}

	classes[suffArr[0]] = 0;
	int classesCounter = 0;//�� 0 ����????

	for (size_t i = 1; i < str.length(); i++) {
		if (str[suffArr[i]] != str[suffArr[i - 1]]) {
			classesCounter++;
		}
		classes[suffArr[i]] = classesCounter;
	}
	std::vector<int> newClasses(str.length(), 0);
	std::vector<int> newSuffArr(str.length(), 0);
	for (size_t degree = 0; degree < logb(str.length()); degree++) {

		for (size_t i = 0; i < str.length(); i++) {
			newSuffArr[i] = suffArr[i] - (1 << degree);
			if (newSuffArr[i] < 0) {
				newSuffArr[i] += str.length();
			}
		}
		letterCounters.assign(classesCounter + 1, 0);

		for (size_t i = 0; i < str.length(); i++) {
			letterCounters[classes[newSuffArr[i]]]++;
		}
		for (size_t i = 1; i < classesCounter + 1; i++) {
			letterCounters[i] += letterCounters[i - 1];
		}
		for (int i = str.length() - 1; i >= 0; --i) {
			suffArr[--letterCounters[classes[newSuffArr[i]]]] = newSuffArr[i];
		}
		newClasses[suffArr[0]] = 0;
		classesCounter = 0;//�� 0 ����????
		for (size_t i = 1; i < str.length(); i++) {
			int part1 = (suffArr[i] + (1 << degree)) % str.length();
			int part2 = (suffArr[i - 1] + (1 << degree)) % str.length();

			if (str[suffArr[i]] != str[suffArr[i - 1]] || classes[part1] != classes[part2]) {
				classesCounter++;
			}
			newClasses[suffArr[i]] = classesCounter;
		}
		classes = newClasses;
	}
	return suffArr;
}

std::vector<int> helper::lcpMassCr(std::string& str, std::vector<int>& suffArr) {
	std::vector<int> lcpMass(str.length());
	std::vector<int> pos(str.length());
	for (int i = 0; i < str.length(); i++) {
		pos[suffArr[i]] = i;
	}
	int k = 0;

	for (int i = 0; i < str.length() - 1; i++) {
		while (str[(i + k) % str.length()] == str[(suffArr[(pos[i] + 1) % str.length()] + k) % str.length()]) {
			k++;
		}
		lcpMass[pos[i]] = k--;
		if (k < 0) {
			k = 0;
		}

	}
	return lcpMass;
}

int helper::getAnswer(std::string str) {
	char c = 'a' - 1;
	str = str + c;
	std::vector<int> suffArr = strToSuffArr(str);
	std::vector<int> lcpMass = lcpMassCr(str, suffArr);
	int answer = str.length()*(str.length() - 1) / 2;
	for (size_t i = 0; i < lcpMass.size(); i++) {
		answer -= lcpMass[i];
	}
	return answer;
}
